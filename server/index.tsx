import App from '../client/App';
import ReactServer from 'react-dom/server';
import React from 'react';
import express from 'express';
import { bootstrapLink } from './cdn-res/bootstrap';
import { meta } from './cdn-res/meta';
import { StaticRouter } from 'react-router-dom';


const app = express();
app.use(express.static('public'));
const salo = ReactServer.renderToString(<StaticRouter><App /></StaticRouter>);

// all routes
app.get('/*', function (_req: any, res: any) {
    res.send(`<!DOCTYPE html>
    <html lang="en">
    <head>
        ${meta}
        ${bootstrapLink}
        <title>SALO</title>
    </head>
    <body>
        <div id="salo">${salo}</div>
        <script type="application/javascript" src="bundle.js" async></script>
    </body>
    </html>`);
});
app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});

// todo for server side
// 1. add routes for pages/main components
// 2. Organize project structure to build ssr chuncks on each route