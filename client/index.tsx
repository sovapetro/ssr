import App from "./App";
import ReactDOM from "react-dom";
import React from "react";
import { BrowserRouter } from "react-router-dom";
import './global.scss';

const salo = document.getElementById('salo');
ReactDOM.hydrate(<BrowserRouter><App /></BrowserRouter>, salo);

// Client TODOs
// 1. add index import file in CoreComponents
// 2. add firebase
// 3. add user preference/ user settings/ user configuration
// 4. user authentification
