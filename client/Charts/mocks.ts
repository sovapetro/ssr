export const CHART_MOCK = [
    {
        "id": "make",
        "label": "make",
        "value": 509,
        "color": "hsl(129, 70%, 50%)"
    },
    {
        "id": "javascript",
        "label": "javascript",
        "value": 533,
        "color": "hsl(263, 70%, 50%)"
    },
    {
        "id": "ruby",
        "label": "ruby",
        "value": 395,
        "color": "hsl(217, 70%, 50%)"
    },
    {
        "id": "stylus",
        "label": "stylus",
        "value": 1,
        "color": "hsl(317, 70%, 50%)"
    },
    {
        "id": "sass",
        "label": "sass",
        "value": 591,
        "color": "hsl(163, 70%, 50%)"
    }
];