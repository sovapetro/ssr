import * as React from 'react';
import { ResponsivePie } from '@nivo/pie'
import { CHART_MOCK } from './mocks';
import { LEGEND, FILL_CONST, DEF_CONST } from './Config';

const MyResponsivePie = (data: any) => (
    <ResponsivePie
        data={data}
        margin={{ top: 40, right: 80, bottom: 80, left: 80 }}
        startAngle={-180}
        innerRadius={0.5}
        padAngle={0.7}
        cornerRadius={3}
        colors={{ scheme: 'pastel1' }}
        borderWidth={1}
        borderColor={{ theme: 'background' }}
        radialLabelsSkipAngle={9}
        radialLabelsTextXOffset={6}
        radialLabelsTextColor="#333333"
        radialLabelsLinkOffset={0}
        radialLabelsLinkDiagonalLength={33}
        radialLabelsLinkHorizontalLength={24}
        radialLabelsLinkStrokeWidth={1}
        radialLabelsLinkColor={{ from: 'color' }}
        slicesLabelsSkipAngle={10}
        slicesLabelsTextColor="#333333"
        animate={true}
        motionStiffness={90}
        motionDamping={15}
        defs={DEF_CONST}
        fill={FILL_CONST}
        legends={LEGEND}
    />
);

export interface IChartExampleProps {
}

export default class ChartExample extends React.Component<IChartExampleProps> {
  public render() {
    return (
      <div style={{height:400}}>
        {MyResponsivePie(CHART_MOCK)}
      </div>
    );
  }
}
