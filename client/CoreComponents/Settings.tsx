import * as React from 'react';

import CurrencyRate from './CurrencyRate/CarrencyRate';

export interface ISeetingsProps {
}

export default class Seetings extends React.Component<ISeetingsProps> {

    public render() {
        return (
            <div>
                <h2>SETTINGS COMPONENT</h2>
                <CurrencyRate />
            </div>
        );
    }
}
