import * as React from 'react';
import ChartExample from '../Charts/ChartExample';

export interface IHomeProps {
}

export default class Home extends React.Component<IHomeProps> {
    public render() {
        return (
            <div>
                <h1>Home Page</h1>
                <ChartExample />
            </div>
        );
    }
}
