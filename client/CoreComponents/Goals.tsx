import * as React from 'react';
import { Toast, ToastHeader, ToastBody } from 'reactstrap';

export interface IGoalsProps {
}

export default class Goals extends React.Component<IGoalsProps> {
    public render() {
        return (
            <div>
                <h2>GOALS COMPONENT</h2>
                <Toast>
                    <ToastHeader icon="primary">
                        Reactstrap
                    </ToastHeader>
                    <ToastBody>
                        This is a toast with a primary icon — check it out!
                    </ToastBody>
                </Toast>
                <Toast>
                    <ToastHeader icon="secondary">
                        Reactstrap
                    </ToastHeader>
                    <ToastBody>
                        This is a toast with a secondary icon — check it out!
                    </ToastBody>
                </Toast>
            </div>
        );
    }
}
