import { ICurrencyRate } from "./CurrencyRate.interface";
const currencyCodes = {
  UAH: 980,
  USD: 840,
};

const finder = (code: any) => (item: { currencyCodeA: string }) =>
  item.currencyCodeA === code;
const API = "https://api.monobank.ua/bank/currency";

// api currency exchange from monobank
export class CurrencyRateService {
    usdToUah(): Promise<ICurrencyRate> {
        return fetch(API)
            .then((resp) => resp.json())
            .then((data: any[]) => {
                const usd_uah = data.find(finder(currencyCodes.USD));
                const base = {
                    currencyA: "USD",
                    currencyB: "UAH",
                };
                console.log(usd_uah);
                return { ...usd_uah, ...base };
            });
    }
}