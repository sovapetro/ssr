import * as React from 'react';
import { CurrencyRateService } from './CurrencyRate.service';
import { ICurrencyRate } from './CurrencyRate.interface';
const service = new CurrencyRateService();

const styles = {
    mimHeight: 50,
    minWidth: 70
};
const textStyles = {
    color: '#7bc74d'
};
const textStyles1 = {
    fontWeight: 700
};

export default class CurrencyRate extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = {
            currencyA: 'USD',
            currencyB: 'UAH',
            sell: 0,
            buy: 0
        };
    }

    public componentDidMount() {
        service.usdToUah().then((currencyRate: ICurrencyRate) => {
            this.setState({
                sell: currencyRate.rateSell,
                buy: currencyRate.rateBuy
            });
        })
    }

    public render() {
        return (
            <div style={styles} className='currency-rate'>
                <div><span style={textStyles1}>{this.state.currencyA}</span> vs <span style={textStyles1}>{this.state.currencyB}</span></div>
                <div>Sell: <span style={textStyles}>{this.state.sell}</span> Buy: <span style={textStyles}>{this.state.buy}</span></div>
            </div>
        );
    }
}
