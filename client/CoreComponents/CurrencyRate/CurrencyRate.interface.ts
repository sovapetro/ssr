export interface ICurrencyRate {
    currencyCodeA: number;
    currencyCodeB: number;
    date: number
    rateBuy: number;
    rateSell: number;
    currencyA: string;
    currencyB: string;
}