import * as React from 'react';
import { Input, Form, FormGroup, Label, Row, Col, CustomInput, Button } from 'reactstrap';

export interface IStrategyProps {
}
// TODO:
// 1. user can type number via numeric or range input (maybe)
const strComponentStyles = {
    margin: '20px auto',
    padding: '12px',
    minWidth: 500,
    width: '85%'
};

interface IState {
    mainGoal: number,
    mode: string,
    bankPercentage: number,
    amountMonthes?: number,
    regularSum?: number
}

export default class Strategy extends React.Component<IStrategyProps, IState> {

    constructor(props: IStrategyProps) {
        super(props);
        // default state
        this.state = {
            mainGoal: 1000000,
            mode: 'time',
            bankPercentage: 12,
            amountMonthes: 12
        };
    }

    handleMainGoalChange = (event: any) => {
        this.setState({ mainGoal: event.target.value });
    }

    calculateResult = () => {
        console.log(this.state, 'calculate result: function ------');
    }

    handleRadioSumChange = () => {
        this.setState({mode: 'sum'}, () => {
            console.log(this.state);
        });

    }

    handleRadioTimeChange = () => {
        this.setState({mode: 'time'}, () => {
            console.log(this.state);
        });
    }

    handleBankPercentage = (event: any) => {
        this.setState({
            bankPercentage: event.target.value
        });
    }

    handleRegularSumChange = (event: any) => {
        this.setState({
            regularSum: event.target.value
        });
    }

    handleAmountMonthesChange = (event: any) => {
        this.setState({
            amountMonthes: event.target.value
        });
    }

    public render() {
        return (
            <div>
                <h2 className="salo-test">STRATEGY COMPONENT</h2>
                <div style={strComponentStyles}>
                    <Form>
                        <Row form>
                            <Col md={2}>
                                <FormGroup>
                                    <Label for="mainGola">Main Goal, uah</Label>
                                    <Input
                                        step={100}
                                        type="number"
                                        name="mainGoal"
                                        id="mainGoal"
                                        onChange={this.handleMainGoalChange}
                                        value={this.state.mainGoal}
                                        placeholder="enter sum" />
                                </FormGroup>
                            </Col>
                            <Col md={8}>
                                <FormGroup>
                                    <Label for="exampleCustomRange">Custom Range</Label><br />
                                    <CustomInput style={{ width: '100%' }} type="range" id="exampleCustomRange" name="customRange" />
                                </FormGroup>
                            </Col>
                            <Col md={2}>
                                <Label for="exampleCustomRange">Custom Range</Label><br />
                                <FormGroup check>
                                    <Label check>
                                        <Input
                                            type="radio"
                                            name="radio1"
                                            onChange={this.handleRadioSumChange}
                                            checked={this.state.mode === 'sum' ? true : false}/>{' '} by sum</Label>
                                </FormGroup>
                                <FormGroup check>
                                    <Label check>
                                        <Input
                                            type="radio"
                                            name="radio1"
                                            onChange={this.handleRadioTimeChange}
                                            checked={this.state.mode === 'time' ? true : false}/>{' '} by time</Label>
                                </FormGroup>
                            </Col>
                        </Row>
                        <hr />
                        <Row form>
                            <Col md={2}>
                                <FormGroup>
                                    <Label for="bankPercentage">Bank percentage, %</Label>
                                    <Input
                                        step={0.1}
                                        type="number"
                                        name="bankPercentage"
                                        onChange={this.handleBankPercentage}
                                        value={this.state.bankPercentage}
                                        id="bankPercentage" />
                                </FormGroup>
                            </Col>
                            <Col md={7}>
                                <FormGroup>
                                    <Label for="exampleState">Description</Label>
                                    <Input type="textarea" name="description" id="description" />
                                </FormGroup>
                            </Col>
                            <Col md={2}>
                                * <br />
                                <span>configuration shows dialog where displayed loan</span>
                            </Col>
                        </Row>
                        <hr />
                        <Row form>
                            <Col md={2}>
                                <FormGroup>
                                    <Label for="bankPercentage">Reqular payment, uah</Label>
                                    <Input
                                        step={1}
                                        disabled={this.state.mode === 'time'}
                                        type="number"
                                        onChange={this.handleRegularSumChange}
                                        value={this.state.regularSum}
                                        name="RegularPayment"
                                        id="RegularPayment" />
                                </FormGroup>
                            </Col>
                            <Col md={7}>
                                <p>Enter number of monthes or regular sum needed to achive goal</p>
                            </Col>
                            <Col md={2}>
                                <FormGroup>
                                    <Label for="bankPercentage">Amount of monthes</Label>
                                    <Input
                                        step={1}
                                        type="number"
                                        name="monthAmount"
                                        onChange={this.handleAmountMonthesChange}
                                        value={this.state.amountMonthes}
                                        disabled={this.state.mode === 'sum'}
                                        id="monthAmount" />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Button
                            onClick={this.calculateResult}
                            color="primary">Calculate
                        </Button>{' '}
                        <hr />
                        <Row>
                            <Col>
                                <h2>Result: present result of the calculation...</h2>
                            </Col>
                        </Row>
                    </Form>
                </div>
            </div>
        );
    }
}
