import * as React from 'react';
import { Nav, NavItem, NavLink, NavbarBrand, Navbar } from 'reactstrap';
import { Switch, Route, NavLink as RRNavLink } from 'react-router-dom';
import Seetings from './CoreComponents/Settings';
import Home from './CoreComponents/Home';
import Strategy from './CoreComponents/Strategy/Strategy';
import Goals from './CoreComponents/Goals';

export interface IAppProps {
}

export default class App extends React.Component<IAppProps> {
    public render() {
        return (
            <div>
                <Navbar color="light" light expand="md">
                    <NavbarBrand to="/" tag={RRNavLink}>Euler finance</NavbarBrand>
                    <Nav>
                        <NavItem>
                            <NavLink to="/strategy" tag={RRNavLink}>Strategy</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink to="/goals" tag={RRNavLink}>Goals</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink to="/settings" tag={RRNavLink}>Settings</NavLink>
                        </NavItem>
                    </Nav>
                </Navbar>
                <div style={{ margin: 10, padding: 10 }}>
                    <Switch>
                        <Route path="/settings" component={Seetings}></Route>
                        <Route exact path="/" component={Home}></Route>
                        <Route path="/strategy" component={Strategy}></Route>
                        <Route path="/goals" component={Goals}></Route>
                    </Switch>
                </div>
            </div>
        );
    }
}
