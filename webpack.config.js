const base = {
    mode: "development",
    resolve: {
        extensions: [".ts", ".tsx", ".js", ".jsx"],
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                exclude: /node_modules/,
                loader: "ts-loader",
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                  // Creates `style` nodes from JS strings
                  'style-loader',
                  // Translates CSS into CommonJS
                  'css-loader',
                  // Compiles Sass to CSS
                  'sass-loader',
                ],
              }
        ],
    }
};

const clientConfig = {
    ...base,
    entry: "./client/index.tsx",
    devtool: "cheap-module-source-map",
    output: {
        path: __dirname,
        filename: "./public/bundle.js",
    },

};

const serverConfig = {
    ...base,
    entry: "./server/index.tsx",
    target: "node",
    output: {
        path: __dirname,
        filename: "./public/server.js",
        libraryTarget: "commonjs2",
    }
};

module.exports = [clientConfig, serverConfig]